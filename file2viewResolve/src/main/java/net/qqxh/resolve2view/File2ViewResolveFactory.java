package net.qqxh.resolve2view;

import net.qqxh.resolve2view.exception.File2ViewResolveNotFondException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class File2ViewResolveFactory {
    @Autowired
    private List<File2ViewResolve> file2ViewResolves;

    public File2ViewResolve getFile2ViewResolve(String fix) throws File2ViewResolveNotFondException {
        File2ViewResolve file2ViewResolve = null;
        for (File2ViewResolve resolve : file2ViewResolves) {
            if (resolve.canResolve(fix)) {
                return resolve;
            }
        }
        throw new File2ViewResolveNotFondException("没有找到view解析器：类型'"+fix+"'请注意是否没有添加注解;");


    }

}
