function Clock(){
	this.today = new Date();
	this.minutes = (this.today.getMinutes() < 10) ? ("0" + this.today.getMinutes()) : this.today.getMinutes();
	this.hour = (this.today.getHours() < 10) ? ("0" + this.today.getHours()) : this.today.getHours();
	this.date = (this.today.getDate() < 10) ? ("0" + this.today.getDate()) : this.today.getDate();
	this.weekdays = ["Sun", "Mon", "Tue","Wed", "Fur", "Fir", "Sat"];
    this.weekdayscn = ["日", "一", "二","三", "四", "五", "六"];
	this.months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    this.monthssn = ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"];
}
Clock.prototype.toString = function() {
	return  this.monthssn[this.today.getMonth()] + " " + this.date + "日, 星期" +this.weekdayscn[this.today.getDay()] + " "+ this.hour + ":" + this.minutes;
}

