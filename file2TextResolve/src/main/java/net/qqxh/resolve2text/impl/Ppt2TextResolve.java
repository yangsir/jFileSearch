package net.qqxh.resolve2text.impl;


import net.qqxh.resolve2text.File2TextResolve;
import org.apache.poi.hslf.extractor.PowerPointExtractor;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
@Component
public class Ppt2TextResolve implements File2TextResolve {
    private static String TYPE="ppt";
    @Override
    public String resolve(byte[] file)  {
        String text = "";
        InputStream fis = null;
        PowerPointExtractor ex = null;
        try {
            fis = new ByteArrayInputStream(file);
            ex = new PowerPointExtractor(fis);
            text = ex.getText();
            ex.close();
        } catch (Exception e) {

            e.printStackTrace();
        }finally {
            if(ex!=null){
                try {
                    ex.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(fis!=null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return text;
    }
    @Override
    public String getType() {
        return TYPE;
    }
}
